from collections import Counter
import pandas as pd
 
def frequentItems(transactions, support):
    counter = Counter()
    for trans in transactions:
        counter.update(frozenset([t]) for t in trans)
    return set(item for item in counter if counter[item]/float(len(transactions)) >= support), counter
 
def generateCandidates(L, k):
    candidates = set()
    for a in L:
        for b in L:
            union = a | b
            if len(union) == k and a != b:
                candidates.add(union)
    return candidates
 
def filterCandidates(transactions, itemsets, support):
    counter = Counter()
    for trans in transactions:
        subsets = [itemset for itemset in itemsets if itemset.issubset(trans)]
        counter.update(subsets)
    return set(item for item in counter if counter[item]/float(len(transactions)) >= support), counter
 
def apriori(transactions, support):
    result = list()
    resultc = Counter()
    candidates, counter = frequentItems(transactions, support)
    result += candidates
    resultc += counter
    k = 2
    while candidates:
        candidates = generateCandidates(candidates, k)
        candidates,counter = filterCandidates(transactions, candidates, support)
        result += candidates
        resultc += counter
        k += 1
    resultc = {item:(resultc[item]/float(len(transactions))) for item in resultc}
    return result, resultc

 
 
def generateDoubles(itemset):
    itemlist = list(itemset)    
    res = []
    for item in itemlist:
        newlist = list(itemlist)
        newlist.remove(item)
        res.append((newlist, item))
    return res


def generateRules(frequentItemsets, supports, minConfidence):
    rules = []

    for itemset in frequentItemsets:
        if len(itemset) < 2:
            continue

        for entry in generateDoubles(itemset):
            leftSide, rightSide = entry
            ruleConfidence = supports[itemset] / supports[frozenset(leftSide)]
            ruleLift = supports[itemset] / (supports[frozenset(leftSide)] * supports[frozenset([rightSide])])
            ruleConviction = (1 - supports[frozenset([rightSide])]) / ((1-supports[itemset] / supports[frozenset(leftSide)]))
            
            if ruleConfidence >= minConfidence:

                rules.append((leftSide, " => ", rightSide, ("Conf = " , ruleConfidence), ("Lift = ", ruleLift), ("Conv = ", ruleConviction)))
    return rules

MIN_LEN_VISIT = 30
MIN_LEN_PAGE = 3

clicks = pd.read_csv("./dataset/clicks.csv")
visitors = pd.read_csv("./dataset/visitors.csv")
print("Clicks: ", len(clicks))
print("Visitors: ", len(visitors))
print("\n")


visitClicks = clicks.merge(visitors, on='VisitID', how="left", )   
print("Before conversion")
conversion = set(["APPLICATION", "CATALOG", "DISCOUNT", "HOWTOJOIN", "INSURANCE", "WHOWEARE"])
for c in conversion:
    print(c + ": " + str(len(visitClicks[visitClicks.PageName == c])))
print("\n") 

visitClicks = visitClicks[visitClicks.Length_seconds > MIN_LEN_VISIT]
visitClicks = visitClicks[visitClicks.Length_pagecount > MIN_LEN_PAGE]
visitClicks = visitClicks[visitClicks.ExtCatID != 1]

print("After conversion")
for c in conversion:
    print(c + ": " + str(len(visitClicks[visitClicks.PageName == c])))
print("\n")

print("Number of visits: " + str(len(visitClicks["VisitID"])))
visitIds = set(visitClicks["VisitID"])
print("Unique visitors: " + str(len(visitIds)))
print("Number of pages: " + str(len(set(visitClicks["PageName"]))))
dataset = []
for visitId in visitIds:
    visitPages = visitClicks[visitClicks.VisitID == visitId]
    row = []
    for visitPage in visitPages.to_dict(orient="records"):
        row.append((visitPage["CatName"] , visitPage["PageName"]))
    dataset.append(row)
print("\n")

frequentItemsets, supports = apriori(dataset, 0.02)
rules = generateRules(frequentItemsets, supports, 0.005)

print("Confidence")
topConfidence = sorted(rules, key=lambda x: x[3], reverse=True)
print(topConfidence[0:10])
print("\n")
print("Lift")
topLift = sorted(rules, key=lambda x: x[4], reverse=True)
print(topLift[0:10])
print("\n")
print("Conviction")
topConviction = sorted(rules, key=lambda x: x[5], reverse=True)
print(topConviction[0:10])