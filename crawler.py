import scrapy
 
class BlogSpider(scrapy.Spider):
    name = 'novinkySpider'
    user_agent = 'DDW'
    download_delay = 1.0
    start_urls = ['https://www.novinky.cz/archiv?id=966']
 
    def parse(self, response):
    	items = response.css('div.item')

        for item in items:
            time = item.css('div.time ::text').extract_first()
            title = item.css('h3.likeInInfo.ilustLink').css('a ::text').extract_first()
            url = item.css('h3.likeInInfo.ilustLink').css('a ::attr(href)').extract_first()
            result  =  scrapy.Request(url, callback=self.parseItem)
            result.meta['item'] = {}
            result.meta['item'].update({'time': time, 'title': title})
            yield result
 
        next_page = response.css('div.fullMonthSwitch').css('a ::attr(href)').extract_first()
        if next_page:
            yield scrapy.Request(response.urljoin(next_page), callback=self.parse)

    def parseItem(self, response):
        item = response.meta['item']
        content = response.css('div.articleBody.articleBody').css('p')
        author = response.css('p.articleAuthors ::text').extract_first()
        section = response.css('h2.pgSection ::text').extract_first()

        text = []
        for p in content:
            text.append(p.css('p ::text').extract_first())
        item.update({'text': "\n".join(text), 'author': author, 'section': section})
        yield item
            

