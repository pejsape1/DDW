import networkx as nx
from networkx.drawing.nx_agraph import graphviz_layout
import matplotlib.pyplot as plt
import numpy as np
import operator
import csv
def readExcel():
    graph = {}
    G = nx.Graph()
    with open("casts.csv", 'r') as csvfile:
        text = csv.reader(csvfile, delimiter=';', quotechar='"')
        for row in text:
            if(row[1] is not "" and row[2] is not "" and row[1] != "s a" and row[2] != "s a" and row[2] != "s a<"): 
                movie = row[1]
                actor = row[2]

                if movie not in graph:
                    graph[movie] = []

                graph[movie].append(actor) # add actor

        for movie in graph:
            for actor in graph[movie]:
                for actor2 in graph[movie]:
                    if (actor is not actor2):
                        G.add_edge(actor, actor2)

    return(G)
def printInfo(G):
    print(nx.info(G))

def printCentralities(G):
    degree = np.array(sorted(nx.degree_centrality(G).items(), key=operator.itemgetter(1), reverse=True))
    print("Degree centrality:")
    print(degree[:10])
    eigenvector = np.array(sorted(nx.eigenvector_centrality(G, max_iter=20).items(), key=operator.itemgetter(1), reverse=True))
    print("Eigenvector centrality")
    print(eigenvector[:10])
    betweenness = np.array(sorted(nx.betweenness_centrality(G, k=20).items(), key=operator.itemgetter(1), reverse=True))
    print("Betweenness centrality")
    print(betweenness[:10])

def printComunities(G):
    print("Density:", nx.density(G))
    print("Number of connected components:", nx.number_connected_components(G))
    print("Size of biggest component: ", len(sorted(nx.connected_components(G), key = len, reverse=True)[0]))
        


def printKevinBacon(G):
    shortes_path = nx.shortest_path_length(G, source = 'Kevin Bacon')
    print("Kevin Bacon number: ", np.mean(list(shortes_path.values())))




G = readExcel()
printInfo(G)
printCentralities(G)
printComunities(G)
printKevinBacon(G)
