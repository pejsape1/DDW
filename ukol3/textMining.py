import nltk
import wikipedia
import string


def extractEntities(ne_chunked):
    data = {}
    for entity in ne_chunked:
        if isinstance(entity, nltk.tree.Tree):
            text = " ".join([word for word, tag in entity.leaves()])
            data[text] = entity.label()
        else:
            continue
    return data


def printWikiExtract(entities):
	for entity in entities:
		try:
			print(entity + " - " + nltk.sent_tokenize(wikipedia.page(entity).summary)[0])
		except:
			print(entity + ' - thing')
		print("")


text = None
with open('text.txt', 'r', encoding="utf8") as f:
    text = f.read()

stops = nltk.corpus.stopwords.words('english')

sentences = nltk.sent_tokenize(text)
tokens = [nltk.word_tokenize(sent) for sent in sentences]
filtred_tokens = [[token for token in sent if (token not in string.punctuation and token not in stops)] for sent in tokens]	
tagged = [nltk.pos_tag(sent) for sent in filtred_tokens]

grammar = "NP: {<DT>?<JJ>*<NN>}"
cp = nltk.RegexpParser(grammar)
my_entities = []
ne_entities_nltk = []
for tag in tagged:
	ne_chunked = nltk.ne_chunk(tag, binary=False)
	ne_entities_nltk.append(extractEntities(ne_chunked))
	my_entities.append(extractEntities(cp.parse(tag)))



res_my_entities = {}
for entity in my_entities:
	for word in entity:
		res_my_entities[word] = entity[word]
res_ne_entities_nltk = {}
for entity in ne_entities_nltk:
	for word in entity:
		res_ne_entities_nltk[word] = entity[word]
	


print ("Ne_chunk entities")
print (res_ne_entities_nltk)
print("")
print("")
print("My entities")
print(res_my_entities)
print("")
print("")
print("WIKIPEDIA")
print ("Ne_chunk entities")
printWikiExtract(res_ne_entities_nltk)
print("")
print("")
print("My entities")
printWikiExtract(res_my_entities)

	